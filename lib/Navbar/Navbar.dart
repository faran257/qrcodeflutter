import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Navbar extends StatelessWidget{
  @override
  Widget build(BuildContext context){
     return LayoutBuilder(
       builder: (context,constraints){
         if(constraints.maxWidth>1200){
           return DesktopNavbar();
         } else if(constraints.maxWidth>800 && constraints.maxWidth<1200){
           return DesktopNavbar();
         } else {
           return MobileNavbar();
         }
       },
     );
  }
}

class DesktopNavbar extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Padding(
      padding: const EdgeInsets.symmetric(vertical:20,horizontal:40),
      child: Container(
      child: Row(
        mainAxisAlignment:MainAxisAlignment.spaceBetween,
        children: [
          Text("FullterQRApp",style:TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.red,
            fontSize: 30
          ),),
          Row (children: [
            Text("Home",style:TextStyle(color: Colors.red),),SizedBox(width:30),
            Text("About",style:TextStyle(color: Colors.red),),SizedBox(width:30),
            Text("Service",style:TextStyle(color: Colors.red),),SizedBox(width:30),
            MaterialButton(
              color: Colors.black,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15.0))),
              onPressed: (){},
              child: Text("Get Started",style:TextStyle(color: Colors.white),),
            ),
          ],)
        ],
      ),
    )
    );
  }
}

class MobileNavbar extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Container(
       child: Column(
         children: [
          Text("FullterQRApp",style:TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.white,
            fontSize: 30
          ),),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child :Row (
            mainAxisAlignment : MainAxisAlignment.center,
            children: [
            Text("Home",style:TextStyle(color: Colors.white),),SizedBox(width:30),
            Text("About",style:TextStyle(color: Colors.white),),SizedBox(width:30),
            Text("Service",style:TextStyle(color: Colors.white),),SizedBox(width:30),
            
          ],)
          ),
        ],
       ),
    );
  }
}